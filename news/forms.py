from django import forms

from .models import Article, ArticleImages

class ArticleForm(forms.ModelForm):
    class Meta:
        fields = "__all__"


class ArticleImagesForm(forms.ModelForm):
    class Meta:
        fields = "__all__"