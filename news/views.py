from django.shortcuts import render
from django.urls import reverse_lazy
from django.views.generic import ListView, DetailView
from django.views.generic.edit import CreateView, DeleteView, UpdateView

#decorators
from django.contrib.auth.decorators import login_required
from django.utils.decorators import method_decorator

from .models import Article, ArticleImages

# Create your views here.

class ArticleListView(ListView):
    model = Article
    template_name = "news/home.html"
    context_object_name = "articles"

class ArticleDetailView(DetailView):
    model = Article
    context_object_name = "article"
    template_name = "news/article_detail.html"

class ArticleCreateForm(CreateView):
    model = Article
    fields = ["title","content"]

    def form_valid(self, form):
        form.instance.publisher = self.request.user
        form.instance.updated_by = self.request.user
        return super().form_valid(form)

@method_decorator(login_required, name='dispatch')
class ArticleUpdateView(UpdateView):
    model = Article
    fields = ["title","content"]

    def form_valid(self, form):
        form.instance.updater = self.request.user
        return super().form_valid(form)

@method_decorator(login_required, name='dispatch')
class ArticleDeleteView(DeleteView):
    model = Article
    success_url = reverse_lazy("news:news_home")