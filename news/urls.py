
from django.urls import path
from . import views

app_name = "news"
urlpatterns = [
    path('', views.ArticleListView.as_view(), name='news_home'),
    path('articles/<int:pk>/', views.ArticleDetailView.as_view(), name="article_details"),
    path('articles/create_article/', views.ArticleCreateForm.as_view(), name="create_article"),
    path('articles/<int:pk>/update/', views.ArticleUpdateView.as_view(), name="update_article"),
    path('articles/<int:pk>/delete/', views.ArticleDeleteView.as_view(), name="delete_article"),
]