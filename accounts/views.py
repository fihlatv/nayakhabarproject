from django.shortcuts import render, redirect
from django.views import View
from django.contrib.auth import authenticate, login, logout

from .forms import LoginForm, RegisterForm
# Create your views here.

class LoginView(View):
    form_class = LoginForm
    template_name = "accounts/login.html"
    
    def post(self, request, *args, **kwargs):
        form = self.form_class(request.POST)
        if form.is_valid():
            username = form.cleaned_data.get("username")
            password = form.cleaned_data.get("password")
            user = authenticate(request, username=username, password=password)
            if user is not None:
                login(request, user)
                return redirect("/")
        return render(self.request, self.template_name, {'form':form})

    def get(self, request, *args, **kwargs):
        form = self.form_class()
        return render(self.request, self.template_name, {'form':form})


# def logout(request):
#     if request.method == "POST":
#         logout(request)