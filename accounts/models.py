from django.db import models
from django.contrib.auth.models import AbstractUser

# Create your models here.
class User(AbstractUser):
    email       = models.EmailField(unique=True)
    nickname    = models.CharField(max_length=50, null=True, blank=True)
    is_subscriber = models.BooleanField(default=False)

    def __str__(self):
        return self.email
    

class Profile(models.Model):
    user        = models.OneToOneField(User, on_delete=models.CASCADE)
    bio         = models.TextField(max_length=500, blank=True)
    address     = models.CharField("Permanent Address", max_length=100, null=True, blank=True)
    birth_date  = models.DateField(auto_now=False, auto_now_add=False, null=True, blank=True)
    position    = models.CharField(max_length=100, null=True, blank=True)

    def __str__(self):
        if self.user:
            return self.user.username
        return "User Profile"
    